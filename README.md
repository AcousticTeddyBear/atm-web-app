# README #
This is a simple ATM Web App.

### What can it do? ###
* Securely login to your account
* View balance
* Debit or credit balance
*  View your last 10 transactions

### Tech Stack ###
* Angular 6
* .NET Core 2.2
* EF Core
* MSSQL

### Requirements ###
* Visual Studio 2019
* SQL Server Express
* Nodejs
* Npm
* AnuglarCLI
* Git

### Notes ###
* Before anything, clone the repository to your local machine using git.
* Once opened in VS2019, you should be able to build the solution with no problems.
* On startup, the database will be automatically created with the default connection string. You can change the connection string in the appsettings.json.
* All passwords are hashed before being saved in the database for security purposes.
* The database will be seeded with 2 accounts. Login will be provided below.
* For authentication, a JWT will be generated, and will be saved into a cache for stateful authentication. However, the JWT will expire after 60 minutes. The auth settings can also be configured in the appsettings.json.

### Accounts ###
Name: John Apilado  
Account Number: 123412341234  
PIN: 123456
____________________________
Name: Tim Espanol  
Account Number: 432143214321  
PIN: 654321