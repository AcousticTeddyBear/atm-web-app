import { Component } from '@angular/core';
import { Router } from '@angular/router';
import Transaction from '../shared/models/transaction';
import { AtmService } from '../shared/services/atm.service';

@Component({
    selector: 'app-view-transactions',
    templateUrl: './view-transactions.component.html'
})

export class ViewTransactionsComponent {
  public transactions: Transaction[];

  constructor(
    private router: Router,
    private atmService: AtmService) {
    atmService.getTransactions().subscribe(resp => this.transactions = resp.data);
  }

  backToMenu() {
    this.router.navigate(['menu']);
  }
}
