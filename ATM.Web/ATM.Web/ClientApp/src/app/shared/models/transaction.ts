import BaseEntity from "./base-entity";

export default interface Transaction extends BaseEntity {
  reference_number: string;
  amount: number;
  transaction_type: string;
  running_balance: number;
}
