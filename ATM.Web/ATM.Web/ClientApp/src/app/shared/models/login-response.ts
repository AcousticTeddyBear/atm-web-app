export default interface LoginResponse {
  token: string;
  expires_in: Date;
  expiry_duration_in_minutes: number;
}
