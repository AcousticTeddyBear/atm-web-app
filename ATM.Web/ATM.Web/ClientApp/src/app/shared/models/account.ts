import BaseEntity from "./base-entity";

export default interface Account extends BaseEntity {
  account_number: string;
  account_name: string;
  running_balance: number;
}
