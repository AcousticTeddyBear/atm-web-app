export default interface BaseEntity {
  created_at: Date;
  modified_at: Date;
}
