import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/internal/operators';
import Account from '../models/account';
import ApiResponse from '../models/api-response';
import LoginResponse from '../models/login-response';
import Transaction from '../models/transaction';


@Injectable()
export class AtmService {
  BASE_URL = 'https://localhost:44304/api';
  AUTH_API = `${this.BASE_URL}/auth`;
  ACCT_API = `${this.BASE_URL}/accounts`;

  constructor(private http: HttpClient) { }

  login(acctNum: string, pin: string): Observable<ApiResponse<LoginResponse>> {
    return this.http.post<ApiResponse<LoginResponse>>(`${this.AUTH_API}/login`, { account_number: acctNum, pin: pin })
      .pipe(tap((resp) => this.saveToken(resp)));
  }

  apiLogout(): Observable<ApiResponse<any>> {
    return this.http.post<ApiResponse<any>>(`${this.AUTH_API}/logout`, null)
      .pipe(tap(() => this.logout()));
  }

  logout() {
    localStorage.removeItem('currentUser')
  }

  getAccount(): Observable<ApiResponse<Account>> {
    return this.http.get<ApiResponse<Account>>(this.ACCT_API);
  }

  debitBalance(amount: number): Observable<ApiResponse<Transaction>> {
    return this.http.post<ApiResponse<Transaction>>(`${this.ACCT_API}/debit`, { amount: amount });
  }

  creditBalance(amount: number): Observable<ApiResponse<Transaction>> {
    return this.http.post<ApiResponse<Transaction>>(`${this.ACCT_API}/credit`, { amount: amount });
  }

  getTransactions(): Observable<ApiResponse<Transaction[]>> {
    return this.http.get<ApiResponse<Transaction[]>>(`${this.ACCT_API}/transactions`);
  }

  private saveToken(resp: ApiResponse<LoginResponse>) {
    if (resp.data) {
      localStorage.setItem('currentUser', JSON.stringify(resp.data));
    }
  }
}
