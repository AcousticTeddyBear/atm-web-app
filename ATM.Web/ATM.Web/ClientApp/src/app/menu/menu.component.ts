import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-menu',
    templateUrl: './menu.component.html',
    styleUrls: ['./menu.component.css']
})
export class MenuComponent {
  constructor(
    private router: Router) {

  }

  public viewBalance() {
    this.router.navigate(['view-balance']);
  }

  public viewTransactions() {
    this.router.navigate(['view-transactions']);
  }

  public debitBalance() {
    this.router.navigate(['adjust-balance', 'Debit']);
  }

  public creditBalance() {
    this.router.navigate(['adjust-balance', 'Credit']);
  }
}
