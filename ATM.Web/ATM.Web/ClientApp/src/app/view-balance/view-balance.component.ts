import { Component } from '@angular/core';
import { Router } from '@angular/router';
import Account from '../shared/models/account';
import { AtmService } from '../shared/services/atm.service';

@Component({
    selector: 'app-view-balance',
    templateUrl: './view-balance.component.html'
})
export class ViewBalanceComponent {
  public account: Account;

  constructor(
    private router: Router,
    private atmService: AtmService) {
    atmService.getAccount().subscribe(resp => this.account = resp.data);
  }

  backToMenu() {
    this.router.navigate(['menu']);
  }
}
