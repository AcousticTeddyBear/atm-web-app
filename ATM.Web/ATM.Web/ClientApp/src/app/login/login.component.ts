import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { AlertService } from '../shared/services/alert.service';
import { AtmService } from '../shared/services/atm.service';


@Component({ templateUrl: 'login.component.html' })
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loading = false;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private atmService: AtmService,
    private alertService: AlertService) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      acctNum: ['', Validators.required],
      pin: ['', Validators.required]
    });

    this.atmService.logout();
  }

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  onSubmit() {
    this.submitted = true;

    if (this.loginForm.invalid) {
      return;
    }

    this.loading = true;
    this.atmService.login(this.f.acctNum.value, this.f.pin.value)
      .pipe(first())
      .subscribe(
        () => {
          window.location.href = `${window.location.protocol}//${window.location.host}/menu`;
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });
  }
}
