import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { AdjustBalanceComponent } from './adjust-balance/adjust-balance.component';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { MenuComponent } from './menu/menu.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { AlertComponent } from './shared/directives/alert.component';
import { AuthGuard } from './shared/guards/auth.guard';
import { ErrorInterceptor } from './shared/helpers/error.interceptor';
import { JwtInterceptor } from './shared/helpers/jwt.interceptor';
import { AlertService } from './shared/services/alert.service';
import { AtmService } from './shared/services/atm.service';
import { ViewBalanceComponent } from './view-balance/view-balance.component';
import { ViewTransactionsComponent } from './view-transactions/view-transactions.component';


@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    AlertComponent,
    LoginComponent,
    MenuComponent,
    ViewBalanceComponent,
    AdjustBalanceComponent,
    ViewTransactionsComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      { path: '', redirectTo: 'login' , pathMatch: 'full' },
      { path: 'login', component: LoginComponent },
      { path: 'menu', component: MenuComponent, pathMatch: 'full', canActivate: [AuthGuard] },
      { path: 'view-balance', component: ViewBalanceComponent, canActivate: [AuthGuard] },
      { path: 'view-transactions', component: ViewTransactionsComponent, canActivate: [AuthGuard] },
      { path: 'adjust-balance/:type', component: AdjustBalanceComponent, canActivate: [AuthGuard] },
      { path: '**', redirectTo: 'menu' }
    ])
  ],
  providers: [
    AuthGuard,
    AlertService,
    AtmService,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
