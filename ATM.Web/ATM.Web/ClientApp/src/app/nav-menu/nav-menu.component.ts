import { Component } from '@angular/core';
import { AtmService } from '../shared/services/atm.service';

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent {
  isExpanded = false;
  isLoggedIn = false;

  constructor(
    private atmService: AtmService
  ) { }

  ngOnInit() {
    if (localStorage.getItem('currentUser')) {
      this.isLoggedIn = true;
    }
  }

  collapse() {
    this.isExpanded = false;
  }

  toggle() {
    this.isExpanded = !this.isExpanded;
  }

  logout() {
    this.atmService.apiLogout()
      .subscribe(() => location.reload(true))
  }
}
