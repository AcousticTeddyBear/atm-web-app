import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';
import ApiResponse from '../shared/models/api-response';
import Transaction from '../shared/models/transaction';
import { AlertService } from '../shared/services/alert.service';
import { AtmService } from '../shared/services/atm.service';

@Component({
    selector: 'app-adjust-balance',
    templateUrl: './adjust-balance.component.html'
})
export class AdjustBalanceComponent {
  adjustBalanceForm: FormGroup;
  loading = false;
  submitted = false;
  transactionType: string;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private atmService: AtmService,
    private alertService: AlertService
  ) { }


  ngOnInit() {
    this.adjustBalanceForm = this.formBuilder.group({
      amount: ['', Validators.required]
    });

    this.route.params.subscribe(params => this.transactionType = params['type']);
  }

  // convenience getter for easy access to form fields
  get f() { return this.adjustBalanceForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.adjustBalanceForm.invalid) {
      return;
    }

    this.loading = true;

    let transaction: Observable<ApiResponse<Transaction>>;

    transaction = this.transactionType == 'Debit' ? this.atmService.debitBalance(this.f.amount.value) : this.atmService.creditBalance(this.f.amount.value);
    transaction
      .pipe(first())
      .subscribe(
        resp => {
          let trans = resp.data;
          alert(`Your account has been ${trans.transaction_type.toLowerCase()}ed PHP ${trans.amount.toFixed(2)}.\nYour current balance is PHP ${trans.running_balance.toFixed(2)}.\nRef#: ${trans.reference_number}`);
          this.backToMenu();
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });
  }

  backToMenu() {
    this.router.navigate(['menu']);
  }
}
