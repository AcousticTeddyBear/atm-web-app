using ATM.Web.Core.Authentication;
using ATM.Web.Core.Data;
using ATM.Web.Core.Data.Repositories;
using ATM.Web.Core.Data.Repositories.Interfaces;
using ATM.Web.Core.Models;
using ATM.Web.Core.Models.Configuration;
using ATM.Web.Core.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SpaServices.AngularCli;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Text;

namespace ATM.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMemoryCache();

            services.AddCors(x => x.AddPolicy("CorsPolicy", builder =>
            {
                builder.AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader();
            }));

            services.AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .AddJsonOptions(x => x.SerializerSettings.NullValueHandling = NullValueHandling.Ignore);

            // In production, the Angular files will be served from this directory
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/dist";
            });

            //Model Validation
            services.Configure((Action<ApiBehaviorOptions>)(options =>
                options.InvalidModelStateResponseFactory = actionContext =>
                {
                    var response = new ApiResponse
                    {
                        Message = string.Join('\n', actionContext.ModelState
                        .Where(x => x.Value.Errors.Any())
                        .Select(x => x.Value.Errors[0].ErrorMessage))
                    };

                    return new BadRequestObjectResult(response);
                }));

            //JWT Authentication
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = "Bearer";
                x.DefaultChallengeScheme = "Bearer";
            }).AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;

                var serviceProvider = services.BuildServiceProvider();
                var authSettings = serviceProvider.GetService<IOptions<AuthSettings>>();
                var signingKey = Encoding.UTF8.GetBytes(authSettings.Value.SecretKey);
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateAudience = false,
                    ValidateIssuer = true,
                    ValidIssuer = authSettings.Value.Issuer,
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(signingKey)
                };
                x.SecurityTokenValidators.Clear();
                x.SecurityTokenValidators.Add(services.BuildServiceProvider().GetService<JwtAuthenticationHandler>());
            });

            services.Configure<AuthSettings>(Configuration.GetSection("AuthSettings"));
            services.AddSingleton(new JwtAuthenticationHandler(services.BuildServiceProvider().GetService<IOptions<AuthSettings>>(), services.BuildServiceProvider().GetService<IMemoryCache>()));
            services.AddDbContext<AtmDbContext>(options =>
            {
                options
                .UseLazyLoadingProxies()
                .UseSqlServer(Configuration.GetConnectionString("AtmDbConnectionString"));
            });
            services.AddScoped<IAccountRepository, AccountRepository>();
            services.AddScoped<ITransactionRepository, TransactionRepository>();
            services.AddScoped<IAccountService, AccountService>();
            services.AddScoped<IAuthenticationService, AuthenticationService>();

            services.AddTransient<DatabaseInitializer>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, DatabaseInitializer dbInitializer)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseCors("CorsPolicy");
            app.UseAuthentication();
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSpaStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller}/{action=Index}/{id?}");
            });

            app.UseSpa(spa =>
            {
                // To learn more about options for serving an Angular SPA from ASP.NET Core,
                // see https://go.microsoft.com/fwlink/?linkid=864501

                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    spa.UseAngularCliServer(npmScript: "start");
                }
            });

            dbInitializer.Initialize().Wait();
        }
    }
}
