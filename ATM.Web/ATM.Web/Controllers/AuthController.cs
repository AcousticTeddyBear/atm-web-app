﻿using ATM.Web.Core.Services;
using ATM.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ATM.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IAuthenticationService _authenticationService;

        public AuthController(IAuthenticationService authenticationService)
        {
            _authenticationService = authenticationService;
        }

        [HttpPost("login")]
        public async Task<IActionResult> Login([FromBody] LoginRequest request)
        {
            var response = await _authenticationService.Login(request.AccountNumber, request.Pin);
            return StatusCode((int)response.StatusCode, response);
        }

        [HttpPost("logout")]
        [Authorize]
        public async Task<IActionResult> Logout()
        {
            var jwt = User.FindFirst(ClaimTypes.Sid).Value;
            var response = await _authenticationService.Logout(jwt);
            return StatusCode((int)response.StatusCode, response);
        }
    }
}
