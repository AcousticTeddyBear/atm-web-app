﻿using ATM.Web.Core.Services;
using ATM.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ATM.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class AccountsController : ControllerBase
    {
        private readonly IAccountService _accountService;

        public AccountsController(IAccountService accountService)
        {
            _accountService = accountService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAccount()
        {
            var accountId = User.FindFirst(ClaimTypes.PrimarySid).Value;
            var response = await _accountService.GetAccount(accountId);
            return StatusCode((int)response.StatusCode, response);
        }

        [HttpGet("transactions")]
        public async Task<IActionResult> GetTransactions()
        {
            var accountId = User.FindFirst(ClaimTypes.PrimarySid).Value;
            var response = await _accountService.GetTransactions(accountId);
            return StatusCode((int)response.StatusCode, response);
        }

        [HttpPost("credit")]
        public async Task<IActionResult> CreditBalance([FromBody] AdjustBalanceRequest request)
        {
            var accountId = User.FindFirst(ClaimTypes.PrimarySid).Value;
            var response = await _accountService.CreditBalance(accountId, request.Amount);
            return StatusCode((int)response.StatusCode, response);
        }

        [HttpPost("debit")]
        public async Task<IActionResult> DebitBalance([FromBody] AdjustBalanceRequest request)
        {
            var accountId = User.FindFirst(ClaimTypes.PrimarySid).Value;
            var response = await _accountService.DebitBalance(accountId, request.Amount);
            return StatusCode((int)response.StatusCode, response);
        }
    }
}
