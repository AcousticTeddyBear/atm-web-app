﻿using System.ComponentModel.DataAnnotations;

namespace ATM.Web.Models
{
    public class AdjustBalanceRequest
    {
        [Required]
        [Range(0.01, double.PositiveInfinity)]
        public double Amount { get; set; }
    }
}
