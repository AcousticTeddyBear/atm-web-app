﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace ATM.Web.Models
{
    public class LoginRequest
    {
        [Required]
        [JsonProperty("account_number")]
        public string AccountNumber { get; set; }

        [Required]
        [JsonProperty("pin")]
        public string Pin { get; set; }
    }
}
