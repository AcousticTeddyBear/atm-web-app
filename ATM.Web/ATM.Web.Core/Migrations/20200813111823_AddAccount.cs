﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ATM.Web.Core.Migrations
{
    public partial class AddAccount : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Accounts",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedAt",
                value: new DateTime(2020, 8, 13, 19, 18, 22, 184, DateTimeKind.Local).AddTicks(8381));

            migrationBuilder.InsertData(
                table: "Accounts",
                columns: new[] { "Id", "AccountId", "AccountName", "AccountNumber", "CreatedAt", "UpdatedAt", "Password", "RunningBalance" },
                values: new object[] { 2, "25080d4c-33c4-4ce9-91fe-a5ebda0eb4e3", "Tim Espanol", "432143214321", new DateTime(2020, 8, 13, 19, 18, 22, 202, DateTimeKind.Local).AddTicks(535), null, "JwT5K7ukL6FjD5DRyVoZOxM8KyRgNebfQ6oX+LoD3sx6/3IgF+lsvdHcSonTLyz0p64ufRPkq1V61I6uX59fWQ==", 10000.0 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Accounts",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.UpdateData(
                table: "Accounts",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedAt",
                value: new DateTime(2020, 8, 12, 11, 0, 1, 212, DateTimeKind.Local).AddTicks(6917));
        }
    }
}
