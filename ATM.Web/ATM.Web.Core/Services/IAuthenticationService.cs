﻿using ATM.Web.Core.Models;
using System.Threading.Tasks;

namespace ATM.Web.Core.Services
{
    public interface IAuthenticationService
    {
        Task<ApiResponse> Login(string accountNumber, string password);

        Task<ApiResponse> Logout(string jwt);
    }
}
