﻿using ATM.Web.Core.Authentication;
using ATM.Web.Core.Data.Repositories;
using ATM.Web.Core.Data.Repositories.Interfaces;
using ATM.Web.Core.Models;
using ATM.Web.Core.Models.Configuration;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Utility = ATM.Web.Core.Utils.Utility;

namespace ATM.Web.Core.Services
{
    public class AuthenticationService : IAuthenticationService
    {
        private readonly JwtAuthenticationHandler _jwtAuthHandler;
        private readonly IAccountRepository _accountRepository;

        public AuthenticationService(JwtAuthenticationHandler jwtAuthHandler, IAccountRepository accountRepository)
        {
            _jwtAuthHandler = jwtAuthHandler;
            _accountRepository = accountRepository;
        }

        public async Task<ApiResponse> Login(string accountNumber, string password)
        {
            var apiResponse = new ApiResponse();
            try
            {
                var account = await _accountRepository.Single(acct => acct.AccountNumber == accountNumber);

                if (account == null || account.Password != Utility.Hash(password, account.AccountId))
                {
                    apiResponse.StatusCode = HttpStatusCode.BadRequest;
                    apiResponse.Message = "Invalid Account Number or PIN.";
                    return apiResponse;
                }

                apiResponse.Data = _jwtAuthHandler.CreateJwtSecurityToken(account.AccountId);
            } catch (Exception e)
            {
                apiResponse.StatusCode = HttpStatusCode.InternalServerError;
                apiResponse.Message = "An error occurred.";
            }

            return apiResponse;
        }

        public async Task<ApiResponse> Logout(string jwt)
        {
            _jwtAuthHandler.Logout(jwt);
            return new ApiResponse();
        }
    }
}
