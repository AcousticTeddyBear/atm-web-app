﻿using ATM.Web.Core.Models;
using System.Threading.Tasks;

namespace ATM.Web.Core.Services
{
    public interface IAccountService
    {
        Task<ApiResponse> GetAccount(string accountId);

        Task<ApiResponse> CreditBalance(string accountId, double amount);

        Task<ApiResponse> DebitBalance(string accountId, double amount);

        Task<ApiResponse> GetTransactions(string accountId);
    }
}
