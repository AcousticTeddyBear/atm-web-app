﻿using ATM.Web.Core.Data.Entities;
using ATM.Web.Core.Data.Repositories.Interfaces;
using ATM.Web.Core.Models;
using ATM.Web.Core.Utils;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ATM.Web.Core.Services
{
    public class AccountService : IAccountService
    {
        private readonly IAccountRepository _accountRepository;
        private readonly ITransactionRepository _transactionRepository;

        public AccountService(IAccountRepository accountRepository, ITransactionRepository transactionRepository)
        {
            _accountRepository = accountRepository;
            _transactionRepository = transactionRepository;
        }

        public async Task<ApiResponse> CreditBalance(string accountId, double amount)
        {
            var apiResponse = new ApiResponse();

            try
            {
                var getAccountResponse = await GetAccount(accountId);
                if (getAccountResponse.StatusCode != HttpStatusCode.OK)
                {
                    return getAccountResponse;
                }

                var account = getAccountResponse.Data as Account;
                account.RunningBalance += amount;

                apiResponse.Data = await adjustBalance(account, amount, TransactionType.Credit);
            }
            catch (Exception e)
            {
                apiResponse.StatusCode = HttpStatusCode.InternalServerError;
                apiResponse.Message = "An error occurred.";
            }

            return apiResponse;
        }

        public async Task<ApiResponse> DebitBalance(string accountId, double amount)
        {
            var apiResponse = new ApiResponse();

            try
            {
                var getAccountResponse = await GetAccount(accountId);
                if (getAccountResponse.StatusCode != HttpStatusCode.OK)
                {
                    return getAccountResponse;
                }

                var account = getAccountResponse.Data as Account;

                if(amount > account.RunningBalance)
                {
                    apiResponse.StatusCode = HttpStatusCode.BadRequest;
                    apiResponse.Message = "Amount is greater than current balance.";
                    return apiResponse;
                }

                account.RunningBalance -= amount;

                apiResponse.Data = await adjustBalance(account, amount, TransactionType.Debit);
            }
            catch (Exception e)
            {
                apiResponse.StatusCode = HttpStatusCode.InternalServerError;
                apiResponse.Message = "An error occurred.";
            }

            return apiResponse;
        }

        public async Task<ApiResponse> GetAccount(string accountId)
        {
            var apiResponse = new ApiResponse();

            try
            {
                apiResponse.Data = await _accountRepository.Single(acct => acct.AccountId == accountId);
                
                if (apiResponse.Data == null)
                {
                    apiResponse.StatusCode = HttpStatusCode.BadRequest;
                    apiResponse.Message = "Account does not exist.";
                }
            } catch(Exception e)
            {
                apiResponse.StatusCode = HttpStatusCode.InternalServerError;
                apiResponse.Message = "An error occurred.";
            }

            return apiResponse;
        }

        public async Task<ApiResponse> GetTransactions(string accountId)
        {
            var apiResponse = new ApiResponse();

            try
            {
                apiResponse.Data = await _transactionRepository
                    .Get(tran => tran.Account.AccountId == accountId)
                    .OrderByDescending(x => x.CreatedAt)
                    .Take(10) //Could have done pagination if there was more time
                    .ToListAsync() ?? new List<Transaction>();
            }
            catch (Exception e)
            {
                apiResponse.StatusCode = HttpStatusCode.InternalServerError;
                apiResponse.Message = "An error occurred.";
            }

            return apiResponse;
        }

        private async Task<Transaction> adjustBalance(Account account, double amount, TransactionType transactionType)
        {
            var transaction = new Transaction
            {
                Type = transactionType,
                Amount = amount,
                RunningBalance = account.RunningBalance,
                Account = account,
                ReferenceNumber = Utility.GenerateReferenceNumber(account.AccountId)
            };

            await _transactionRepository.Add(transaction);
            await _accountRepository.Update(account);

            return transaction;
        }
    }
}
