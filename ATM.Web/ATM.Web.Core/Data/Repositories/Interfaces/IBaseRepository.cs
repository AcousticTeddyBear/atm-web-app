﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ATM.Web.Core.Data.Repositories.Interfaces
{
    public interface IBaseRepository<TEntity> where TEntity : class
    {
        IQueryable<TEntity> Get(Expression<Func<TEntity, bool>> predicate = null);

        Task<TEntity> Single(Expression<Func<TEntity, bool>> predicate);

        Task Add(TEntity entity);

        Task Add(IEnumerable<TEntity> entities);

        Task Update(TEntity entity);

        Task Delete(TEntity entity);
    }
}
