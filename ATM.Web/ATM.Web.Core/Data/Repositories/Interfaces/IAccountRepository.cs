﻿using ATM.Web.Core.Data.Entities;

namespace ATM.Web.Core.Data.Repositories.Interfaces
{
    public interface IAccountRepository : IBaseRepository<Account>
    {

    }
}
