﻿using ATM.Web.Core.Data.Entities;
using ATM.Web.Core.Data.Repositories.Interfaces;

namespace ATM.Web.Core.Data.Repositories
{
    public class TransactionRepository : BaseRepository<Transaction>, ITransactionRepository
    {
        public TransactionRepository(AtmDbContext dbContext) : base(dbContext)
        {
        }
    }
}
