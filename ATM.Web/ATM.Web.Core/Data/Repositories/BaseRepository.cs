﻿using ATM.Web.Core.Data.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ATM.Web.Core.Data.Repositories
{
    public abstract class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : class
    {
        private readonly DbContext _dbContext;

        public BaseRepository(DbContext dbContext) => _dbContext = dbContext;

        public IQueryable<TEntity> Get(Expression<Func<TEntity, bool>> predicate)
        {
            var entities = _dbContext.Set<TEntity>() as IQueryable<TEntity>;

            if (predicate != null)
            {
                entities = entities.Where(predicate);
            }

            return entities;
        }

        public Task<TEntity> Single(Expression<Func<TEntity, bool>> predicate) => _dbContext.Set<TEntity>().SingleOrDefaultAsync(predicate);

        public Task Add(TEntity entity)
        {
            _dbContext.Set<TEntity>().Add(entity);
            return _dbContext.SaveChangesAsync();
        }

        public Task Add(IEnumerable<TEntity> entities)
        {
            _dbContext.Set<TEntity>().AddRange(entities);
            return _dbContext.SaveChangesAsync();
        }

        public Task Update(TEntity entity)
        {
            _dbContext.Set<TEntity>().Update(entity);
            return _dbContext.SaveChangesAsync();
        }

        public Task Delete(TEntity entity)
        {
            _dbContext.Set<TEntity>().Remove(entity);
            return _dbContext.SaveChangesAsync();
        }
    }
}
