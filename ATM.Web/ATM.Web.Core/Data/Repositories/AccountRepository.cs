﻿using ATM.Web.Core.Data.Entities;
using ATM.Web.Core.Data.Repositories.Interfaces;

namespace ATM.Web.Core.Data.Repositories
{
    public class AccountRepository : BaseRepository<Account>, IAccountRepository
    {
        public AccountRepository(AtmDbContext dbContext) : base(dbContext)
        {
        }
    }
}
