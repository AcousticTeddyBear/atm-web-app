﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace ATM.Web.Core.Data
{
    public class DatabaseInitializer
    {
        private readonly AtmDbContext _context;
        private readonly IConfiguration _configuration;
        public DatabaseInitializer(AtmDbContext context, IConfiguration configuration)
        {
            _context = context;
            _configuration = configuration;
        }

        public async Task Initialize()
        {
            await _context.Database.MigrateAsync();
        }
    }
}
