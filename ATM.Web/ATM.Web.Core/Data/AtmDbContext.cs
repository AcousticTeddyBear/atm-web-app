﻿using ATM.Web.Core.Data.Entities;
using ATM.Web.Core.Utils;
using Castle.DynamicProxy.Contributors;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ATM.Web.Core.Data
{
    public class AtmDbContext : DbContext
    {
        public AtmDbContext(DbContextOptions<AtmDbContext> options) : base(options)
        {

        }

        public DbSet<Account> Accounts { get; set; }

        public DbSet<Transaction> Transactions { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Transaction>().Property(x => x.Type).HasConversion<short>();

            var acctId = "ff39ba15-814b-426f-992d-d8b62039ea36";
            var hashedPin = Utility.Hash("123456", acctId);
            var account = new Account
            {
                Id = 1,
                AccountId = acctId,
                AccountNumber = "123412341234",
                AccountName = "John Apilado",
                RunningBalance = 50000,
                CreatedAt = DateTime.Now,
                Password = hashedPin
            };

            modelBuilder.Entity<Account>().HasData(account);

            acctId = "25080d4c-33c4-4ce9-91fe-a5ebda0eb4e3";
            hashedPin = Utility.Hash("654321", acctId);
            account = new Account
            {
                Id = 2,
                AccountId = acctId,
                AccountNumber = "432143214321",
                AccountName = "Tim Espanol",
                RunningBalance = 10000,
                CreatedAt = DateTime.Now,
                Password = hashedPin
            };

            modelBuilder.Entity<Account>().HasData(account);

        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            addTimestamps();
            return base.SaveChangesAsync(cancellationToken);
        }

        private void addTimestamps()
        {
            var entities = ChangeTracker.Entries().Where(x => x.Entity is BaseEntity && (x.State == EntityState.Added || x.State == EntityState.Modified));
            var timestamp = DateTime.Now;

            foreach (var entity in entities)
            {
                switch (entity.State)
                {
                    case EntityState.Modified:
                        ((BaseEntity)entity.Entity).ModifiedAt = timestamp;
                        break;
                    case EntityState.Added:
                        ((BaseEntity)entity.Entity).CreatedAt = timestamp;
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
