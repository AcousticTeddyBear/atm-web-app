﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ATM.Web.Core.Data.Entities
{
    [Table("Transactions")]
    public class Transaction : BaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [JsonIgnore]
        public int Id { get; set; }

        [Column]
        [JsonProperty("reference_number")]
        public string ReferenceNumber { get; set; }

        [Column]
        [JsonProperty("amount")]
        public double Amount { get; set; }

        [Column]
        [JsonConverter(typeof(StringEnumConverter))]
        [JsonProperty("transaction_type")]
        public TransactionType Type { get; set; }

        [Column]
        [JsonProperty("running_balance")]
        public double RunningBalance { get; set; }

        [JsonIgnore]
        public virtual Account Account { get; set; }
    }
}
