﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ATM.Web.Core.Data.Entities
{
    [Table("Accounts")]
    public class Account : BaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [JsonIgnore]
        public int Id { get; set; }

        [Column]
        [JsonIgnore]
        public string AccountId { get; set; }

        [Column]
        [JsonProperty("account_number")]
        public string AccountNumber { get; set; }

        [Column]
        [JsonProperty("account_name")]
        public string AccountName { get; set; }

        [Column]
        [JsonIgnore]
        public string Password { get; set; }

        [Column]
        [JsonProperty("running_balance")]
        public double RunningBalance { get; set; }
    }
}
