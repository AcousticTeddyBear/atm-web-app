﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ATM.Web.Core.Data.Entities
{
    public class BaseEntity
    {
        [Column("CreatedAt")]
        [JsonProperty("created_at")]
        public DateTime CreatedAt { get; set; }

        [Column("UpdatedAt")]
        [JsonProperty("updated_at")]
        public DateTime? ModifiedAt { get; set; }
    }
}
