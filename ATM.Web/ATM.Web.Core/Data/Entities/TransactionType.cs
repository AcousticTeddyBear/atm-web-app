﻿namespace ATM.Web.Core.Data.Entities
{
    public enum TransactionType
    {
        Debit, Credit
    }
}