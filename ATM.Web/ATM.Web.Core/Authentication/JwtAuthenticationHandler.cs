﻿using ATM.Web.Core.Models;
using ATM.Web.Core.Models.Configuration;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Authentication;
using System.Security.Claims;
using System.Text;

namespace ATM.Web.Core.Authentication
{
    public class JwtAuthenticationHandler : JwtSecurityTokenHandler
    {
        private readonly AuthSettings _authSettings;
        private readonly IMemoryCache _cache;

        public JwtAuthenticationHandler(IOptions<AuthSettings> authSettings, IMemoryCache cache)
        {
            _authSettings = authSettings.Value;
            _cache = cache;
        }

        public LoginResponse CreateJwtSecurityToken(string accountId)
        {
            var key = Encoding.UTF8.GetBytes(_authSettings.SecretKey);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Issuer = _authSettings.Issuer,
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.PrimarySid, accountId)
                }),
                Expires = DateTime.Now.AddMinutes(_authSettings.ExpiryInMinutes),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            var token = CreateToken(tokenDescriptor);
            var tokenString = WriteToken(token);

            _cache.Set(tokenString, accountId, new DateTimeOffset(tokenDescriptor.Expires.Value));

            return new LoginResponse
            {
                Token = tokenString,
                ExpiresIn = tokenDescriptor.Expires,
                ExpiryDurationInMinutes = _authSettings.ExpiryInMinutes
            };
        }

        public override ClaimsPrincipal ValidateToken(string token, TokenValidationParameters validationParameters, out SecurityToken validatedToken)
        {
            var claimsPrincipal = base.ValidateToken(token, validationParameters, out validatedToken);

            var accountId = _cache.Get(token) as string;
            if(accountId == null || accountId != claimsPrincipal.FindFirst(ClaimTypes.PrimarySid)?.Value)
            {
                throw new AuthenticationException("Invalid token.");
            }

            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Sid, token)
            };

            var claimsIdentity = new ClaimsIdentity(claims);
            claimsPrincipal.AddIdentity(claimsIdentity);

            return claimsPrincipal;
        }

        public void Logout(string jwt) => _cache.Remove(jwt);
    }
}
