﻿namespace ATM.Web.Core.Models.Configuration
{
    public class AuthSettings
    {
        public string SecretKey { get; set; }
        public int ExpiryInMinutes { get; set; }
        public string Issuer { get; set; }
    }
}
