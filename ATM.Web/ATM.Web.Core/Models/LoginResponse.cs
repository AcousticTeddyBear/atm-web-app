﻿using Newtonsoft.Json;
using System;

namespace ATM.Web.Core.Models
{
    public class LoginResponse
    {
        [JsonProperty("token")]
        public string Token { get; set; }

        [JsonProperty("expires_in")]
        public DateTime? ExpiresIn { get; set; }

        [JsonProperty("expiry_duration_in_minutes")]
        public int ExpiryDurationInMinutes { get; set; }
    }
}
