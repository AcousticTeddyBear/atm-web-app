﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace ATM.Web.Core.Utils
{
    public static class Utility
    {
        public static string Hash(string plainText, string salt)
        {
            var derivationKey = new Rfc2898DeriveBytes(plainText, Encoding.UTF8.GetBytes(salt), 10000);
            var saltBytes = derivationKey.GetBytes(32);

            var dataBytes = Encoding.UTF8.GetBytes(plainText);

            Array.Resize(ref saltBytes, saltBytes.Length + dataBytes.Length);
            Array.Copy(dataBytes, 0, saltBytes, 32, dataBytes.Length);

            using (SHA512 sha512 = new SHA512CryptoServiceProvider())
            {
                var hash = sha512.ComputeHash(saltBytes);
                return Convert.ToBase64String(hash);
            }
        }

        public static string GenerateReferenceNumber(string accountId)
        {
            var prefix = accountId.Substring(0, 8);
            var suffix = Guid.NewGuid().ToString().Substring(0, 8);
            var dateTimeString = DateTime.Now.ToString("yyyyMMdd");

            return $"{prefix}-{dateTimeString}-{suffix}";
        }
    }
}
